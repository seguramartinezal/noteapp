import { GoSearch, BsTrash } from "react-icons/all";
import { Container, Nav, Navbar, NavbarBrand, Form } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import React from "react";
import "./Note.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Menu, Button, Dropdown } from "antd";
import "./Menu.css";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    PieChartOutlined,
    HeartOutlined,
    DeleteOutlined,
    PoweroffOutlined,
    FileOutlined
} from '@ant-design/icons';
import axios from "axios";
import Mansory from "react-masonry-css";
import { ToastContainer, toast } from "react-toastify";


export const URL = "http://localhost:4000/Trash";
//let idsession = sessionStorage.getItem('ID');
let idsession;


const initialState = {
    Title: "",
    Note: [],
    Result: Boolean,
    data: [],
}


export default class Trash extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            show: false,
            collapsed: false,
            activeKey: null,
            data: [],
            Title: "",
            Note: [],
            Result: Boolean,
            loadTitle: String,
            loadNote: String,
            loadId: Number,
            modal: false,
            closeButtonModal: false,
            datacopy: [],
            session: true
        }
    }

    componentDidMount() {
        this.getAllData();
    }


    getAllData = () => {

        axios.get(URL).then(response => {
            this.setState({ data: response.data.rows, datacopy: response.data.rows });
        });


    }


    toggleCollapsed = () => {
        this.setState({ collapsed: !this.state.collapsed });
    }

    Handle = () => {
        this.setState({ show: !this.state.show });
    }

    handleClick = (e) => {
        this.setState({
            activeKey: e
        });
    }


    removeTrash = async (obj) => {
        let idsession = sessionStorage.getItem('ID');
        const NoteInfo = {
            id: obj.ID,
            place: 0,
            idUser: idsession
        }


        let res = await axios.put("http://localhost:4000/Trash/", NoteInfo);
        this.setState({ Result: res.data });
        this.getAllData();
        this.reactNotificationTrash();
        this.resetState();
    }


    addFavorite = async (obj) => {
        let idsession = sessionStorage.getItem('ID');
        const NoteInfo = {
            id: obj.ID,
            place: 1,
            idUser: idsession
        }


        let res = await axios.put("http://localhost:4000/Trash/", NoteInfo);
        this.setState({ Result: res.data });
        this.getAllData();
        this.reactNotificationFavorite();
        this.resetState();

    }


    deleteNote = async (obj) => {

        const ID = obj.ID;

        let res = await axios.delete("http://localhost:4000/Trash/", { data: { id: ID } });
        this.setState({ Result: res.data });
        this.getAllData();
        this.reactNotificationDeleteNote();
        this.resetState();

    }

    filterNote = (event) => {
        event.preventDefault();
        let text = event.target.value.toLowerCase();
        let arrayData = [];

        if (text) {

            arrayData = this.state.data.filter((record) => record.Titulo.toLowerCase().includes(text) || record.Contenido.toLowerCase().includes(text));

            this.setState({ data: arrayData });

        } else {

            this.setState({ data: this.state.datacopy });
        }


    }

    closeSession = () => {

        if (this.state.session) {
            idsession = sessionStorage.removeItem('ID');
            this.setState({ session: false });

        }
    }




    reactNotificationDeleteNote = () => {

        this.state.Result === true ? toast("Nota borrada correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error.", { autoClose: 3000, type: "info", theme: "dark" });
    }


    reactNotificationTrash = () => {

        this.state.Result === true ? toast("Nota removida correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error.", { autoClose: 3000, type: "info", theme: "dark" });
    }

    reactNotificationFavorite = () => {

        this.state.Result === true ? toast("Nota agregada a favorito.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error", { autoClose: 2000, type: "info", theme: "dark" });
    }

    resetState = () => {
        this.setState(initialState);
    }




    render() {


        if (this.state.session) {


            if (this.state.data.length !== 0) {

                return (
                    <div>
                        <ToastContainer />
                        <Navbar bg="dark" variant="dark">

                            <Container id="Contenedor">
                                <NavbarBrand>NoteApp</NavbarBrand>
                                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton">
                                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                                </Button>


                                <Nav className="me-auto">

                                </Nav>

                                <Form>

                                    <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} />
                                    </button>

                                    <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" name="filter" onKeyUp={this.filterNote} />
                                </Form>

                            </Container>
                        </Navbar>


                        <div style={{ width: 256 }}>



                            <Menu defaultSelectedKeys={['3']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>


                                <Menu.Item key="1" icon={<PieChartOutlined />}> <Link to="/Notes"> Notas </Link> </Menu.Item>

                                <Menu.Item key="2" icon={<HeartOutlined />}> <Link to="/Favorites"> Favoritos </Link> </Menu.Item>

                                <Menu.Item key="3" icon={<DeleteOutlined />}> Papelera </Menu.Item>
                                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>


                            </Menu>



                        </div>


                        {this.state.data.map((info, id) =>
                            <Mansory className="my-masonry-grid" columnClassName="my-masonry-grid_column" key={id} >

                                <div className="row"  >
                                    <div>

                                        <div id="id" >{info.ID}</div>
                                        <div className="artigo_nome1">{info.Titulo}</div>
                                        <div className="artigo_nome" >{info.Contenido}</div>

                                    </div>
                                    <Dropdown.Button overlay={

                                        <Menu>
                                            <Menu.Item onClick={() => this.addFavorite(info)} key={1} icon={<HeartOutlined />}>Agregar a favorito</Menu.Item>
                                            <Menu.Item onClick={() => this.removeTrash(info)} key={2} icon={<FileOutlined />}>Mover a notas</Menu.Item>
                                            <Menu.Item onClick={() => this.deleteNote(info)} key={3} icon={<DeleteOutlined />}>Borrar</Menu.Item>
                                        </Menu>
                                    } type="text" className="MenuButton"></Dropdown.Button>

                                </div>


                            </Mansory>
                        )}


                    </div>
                );
            } else {

              
                return (
                    <div>
                        <ToastContainer />
                        <Navbar bg="dark" variant="dark">

                            <Container id="Contenedor">
                                <NavbarBrand>NoteApp</NavbarBrand>
                                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton">
                                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                                </Button>


                                <Nav className="me-auto">

                                </Nav>

                                <Form>

                                    <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} />
                                    </button>

                                    <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" name="filter" />
                                </Form>

                            </Container>
                        </Navbar>


                        <div style={{ width: 256 }}>



                            <Menu defaultSelectedKeys={['3']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>


                                <Menu.Item key="1" icon={<PieChartOutlined />}> <Link to="/Notes"> Notas </Link> </Menu.Item>

                                <Menu.Item key="2" icon={<HeartOutlined />}> <Link to="/Favorites"> Favoritos </Link> </Menu.Item>

                                <Menu.Item key="3" icon={<DeleteOutlined />}> Papelera </Menu.Item>
                                {/* <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item> */}
                                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>


                            </Menu>


                        </div>

                        {/* <Menu defaultSelectedKeys={['3']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>
                        <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>

                        </Menu> */}

        

                        <div id="Title">
                            {/* <BsTrash size={55} /> */}
                            <h2 > No hay notas en la papelera</h2>
                        </div>

  
                       

                    </div>
                )

            }
        
        } else {
            return (
                <Redirect to="/" />
            )
        }

    }

}
