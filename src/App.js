
import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Formulario from './Form';
import Registro from './Registration';
import NoteBook from './Note';
import Favorites from './Favorites';
import Trash from './Trash';


function App() {
  return (
    <div className="App">
     <Layout/>
      
    </div>
  );
}


function Layout(){

  return (

    <div>
      
    <Router>

      <Switch>
      <Route exact path='/' component={Formulario}/>
      <Route exact path='/Registro' component={Registro}/>
      <Route exact path='/Notes' component={NoteBook}/>
      <Route exact path='/Favorites' component={Favorites}/>
      <Route exact path='/Trash' component={Trash}/>
      
      </Switch>

    </Router>

    </div>
  )
}

export default App;