import React from "react";
import { Form } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Form.css";
import axios from "axios";
import {ToastContainer, toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export const URL = "http://localhost:4000";
export let IDusuario;
export let id;


export default class Formulario extends React.Component {


    constructor(props) {
        super(props)
        this.state = { User: String, Password: String, Responsex: Boolean}
    }

    handleInputInfo = ({ target }) => {
        const { name, value } = target;
        this.setState({ [name]: value })
    }

    handleForm = async (event) => {
        event.preventDefault();

        let objInfo = {
            User: this.state.User,
            Password: this.state.Password
        }

        let response = await axios.post(URL, objInfo);
        this.setState({ Responsex: response.data });
        IDusuario = response.data.id;
        sessionStorage.setItem('ID', IDusuario);
    //    id = localStorage.setItem('ID', IDusuario);
    //     console.log(id);
        this.reactNotification();

    }


    reactNotification = () => {

        this.state.Responsex.data === true ?
            toast("Bienvenido.", {autoClose:3000, type:"success", theme:"dark"}) :
            toast("Datos Incorrectos.", {autoClose:3000, type:"error", theme:"dark"});
        
    }

   


    render() {

        if (this.state.Responsex.data === true) {

            return (

                <div>

                <Redirect to="/Notes"/>
             
                </div>
            )

        } else {

            return (

                <div>

                    <div id="ContenedorForm">
                        <h2 id="TituloForm">Iniciar Sesion</h2>
                        <form onSubmit={this.handleForm} className="form control">

                            <b><i>Nombre de Usuario</i></b> <Form.Control type="text" name="User" id="input"
                                onChange={this.handleInputInfo}/> <br />
                            
                            <b><i>Contraseña</i></b>  <Form.Control type="text" name="Password" id="input"
                                onChange={this.handleInputInfo} /><br />

                            <div className="buttoncrear">
                                <Link to={"./Registro"} style={{ color: 'white', textDecoration: 'none' }}>
                                    <button className="btn btn-success">Crear Cuenta</button>
                                </Link>
                            </div>

                            <div className="buttoniniciar" >
                                {/* <Link to={"./Notes"} style={{ color: 'white', textDecoration: 'none' }}>
                                <button className="btn btn-primary" type="submit" >Iniciar Sesion</button>
                            </Link> */}


                                <button className="btn btn-primary" type="submit" >Iniciar Sesion</button>
                            
                            </div>

                        </form>

                    </div>
                    <ToastContainer/>
                </div>
            )
        }
    }

}