import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Menu, Button, Dropdown, Modal } from "antd";
import "./Menu.css";
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    PieChartOutlined,
    DeleteOutlined,
    PoweroffOutlined,
    HeartOutlined
} from '@ant-design/icons';
import { GoSearch, AiOutlineFile } from "react-icons/all";
import { Container, Navbar, NavbarBrand, Form } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import "./Favorites.css";
import axios from "axios";
import Mansory from "react-masonry-css";
import { ToastContainer, toast } from "react-toastify";

export const URL = "http://localhost:4000/Favorites";
const URLuser = "http://localhost:4000/getid";


const initialStateModal = {
    loadTitle: String,
    loadNote: String,
    loadId: Number
}

const initialState = {
    Title: "",
    Note: [],
    Result: Boolean,
    data: [],
}


export default class Favorites extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            show: false,
            collapsed: false,
            activeKey: null,
            data: [],
            Title: "",
            Note: [],
            Result: Boolean,
            loadTitle: String,
            loadNote: String,
            loadId: Number,
            modal: false,
            closeButtonModal: false,
            datacopy: [],
            session: true,
            idusuario: Number

        }
    }


    componentDidMount() {
        this.getAllData();
    }


    getAllData = async () => {
       

        axios.get(URL).then(response => {
            this.setState({ data: response.data.rows, datacopy: response.data.rows });
        });

    }


    // getAllData = async () => {
    //     await axios.get(URLuser).then(response => {
    //      this.setState({idusuario: response.data.id});
    //    });
      
   
    //    await axios.get(URL, {params: {id: this.state.idusuario}}).then(response => {
    //      this.setState({ data: response.data.rows, datacopy: response.data.rows });
    //    });
   
   
    //  }




    toggleCollapsed = () => {
        this.setState({ collapsed: !this.state.collapsed });
    }

    Handle = () => {
        this.setState({ show: !this.state.show });
    }

    handleClick = (e) => {
        this.setState({
            activeKey: e
        });
    }



    showModal = (obj) => {
        this.loadDataModal(obj);
        this.setState({ modal: !this.state.modal });


    }

    loadDataModal = (obj) => {
        this.setState({ loadId: obj.ID, loadTitle: obj.Titulo, loadNote: obj.Contenido, Title: obj.Titulo, Note: obj.Contenido });

    }

    updateDataNote = async () => {

        const NoteInfo = {
            id: this.state.loadId,
            title: this.state.loadTitle,
            content: this.state.loadNote
        }

        if (this.state.Title.length !== this.state.loadTitle.length ||
            this.state.Note.length !== this.state.loadNote.length) {

            let res = await axios.put("http://localhost:4000/Favorites/:?id=" + this.state.loadId, NoteInfo);
            this.setState({ Result: res.data });
            this.getAllData();
            this.reactNotificationModal();
            this.resetState();
        }
    }


    removeFavorite = async (obj) => {
        let idsession = sessionStorage.getItem('ID');
        const NoteInfo = {
            id: obj.ID,
            place: 0,
            idUser: idsession
        }


        let res = await axios.put("http://localhost:4000/Favorites/", NoteInfo);
        this.setState({ Result: res.data });
        this.getAllData();
        this.reactNotificationFavorite();
        this.resetState();

    }

    addTrash = async (obj) => {
        let idsession = sessionStorage.getItem('ID');

        const NoteInfo = {
            id: obj.ID,
            place: 2,
            idUser: idsession
        }


        let res = await axios.put("http://localhost:4000/Notes/", NoteInfo);
        this.setState({ result: res.data });
        this.getAllData();
        this.reactNotificationTrash();
        this.resetState();

    }





    handleOk = () => {
        this.updateDataNote();
        this.setState({ modal: !this.state.modal });
    };


    handleCancel = () => {

        this.setState({ modal: !this.state.modal });
        this.setState(initialStateModal);

    };


    handleChange = ({ target }) => {
        const { name, value } = target;
        this.setState({ [name]: value });

    }


    filterNote = (event) => {
        event.preventDefault();
        let text = event.target.value.toLowerCase();
        let arrayData = [];

        if (text) {

            arrayData = this.state.data.filter((record) => record.Titulo.toLowerCase().includes(text) || record.Contenido.toLowerCase().includes(text));

            this.setState({ data: arrayData });

        } else {

            this.setState({ data: this.state.datacopy });
        }

    }

    closeSession = () => {
        let idsession;
        if (this.state.session) {
             idsession = sessionStorage.removeItem('ID');
            this.setState({ session: false });

        }
    }



    reactNotificationTrash = () => {

        this.state.result === true ? toast("Nota agregada a la papelera.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error", { autoClose: 2000, type: "info", theme: "dark" });
    }

    reactNotificationFavorite = () => {

        this.state.Result === true ? toast("Nota removida correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error.", { autoClose: 3000, type: "info", theme: "dark" });
    }


    reactNotificationModal = () => {

        this.state.Result === true ? toast("Nota actualizada correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error.", { autoClose: 3000, type: "info", theme: "dark" });
    }

    resetState = () => {
        this.setState(initialState);
    }

    render() {

        if (this.state.session) {


            if (this.state.data.length !== 0) {

                return (
                    <div>
                        <ToastContainer />

                        <Navbar bg="dark" variant="dark">

                            <Container id="Contenedor">
                                <NavbarBrand>NoteApp</NavbarBrand>
                                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton1">
                                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                                </Button>


                                <Form>


                                    <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} /> </button>


                                    <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" name="filter" onKeyUp={this.filterNote} />
                                </Form>


                            </Container>
                        </Navbar>



                        <div style={{ width: 256 }}>



                            <Menu defaultSelectedKeys={['2']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>


                                <Menu.Item key="1" icon={<PieChartOutlined />}><Link to="/Notes"> Notas </Link> </Menu.Item>
                                <Menu.Item key="2" icon={<HeartOutlined />}>Favoritos</Menu.Item>
                                <Menu.Item key="3" icon={<DeleteOutlined />}> <Link to="/Trash"> Papelera </Link>  </Menu.Item>
                                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>


                            </Menu>


                        </div>


                        {this.state.data.map((info, id) =>
                            <Mansory className="my-masonry-grid" columnClassName="my-masonry-grid_column" key={id} >

                                <div className="row"  >
                                    <div onClick={() => this.showModal(info)}>

                                        <div id="id" >{info.ID}</div>
                                        <div className="artigo_nome1">{info.Titulo}</div>
                                        <div className="artigo_nome" >{info.Contenido}</div>

                                    </div>
                                    <Dropdown.Button overlay={

                                        <Menu>
                                            <Menu.Item onClick={() => this.removeFavorite(info)} key={1} icon={<HeartOutlined />}>Quitar de favorito</Menu.Item>
                                            <Menu.Item onClick={() => this.addTrash(info)} key={2} icon={<DeleteOutlined />}>Mover a la papelera</Menu.Item>
                                        </Menu>
                                    } type="text" className="MenuButton"></Dropdown.Button>

                                </div>


                            </Mansory>
                        )}


                        <ModalMenu visible={this.state.modal} onOk={this.handleOk} onCancel={this.handleCancel} closable={this.state.closeButtonModal} onChange={this.handleChange}>

                            <textarea type="text" placeholder="Titulo" name="loadTitle" onChange={this.handleChange} autoFocus autoComplete="off" value={this.state.loadTitle} /><br /><br />

                            <textarea type="text" placeholder="Tomar una nota..." name="loadNote" onChange={this.handleChange} autoComplete="off" value={this.state.loadNote} /><br />


                        </ModalMenu>




                    </div>
                )
            } else {

                return (
                    <div>
                        <ToastContainer />
                        <Navbar bg="dark" variant="dark">

                            <Container id="Contenedor">
                                <NavbarBrand>NoteApp</NavbarBrand>
                                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton1">
                                    {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                                </Button>


                                <Form>


                                    <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} /> </button>


                                    <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" />
                                </Form>


                            </Container>
                        </Navbar>



                        <div style={{ width: 256 }}>



                            <Menu defaultSelectedKeys={['2']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>


                                <Menu.Item key="1" icon={<PieChartOutlined />}><Link to="/Notes"> Notas </Link> </Menu.Item>
                                <Menu.Item key="2" icon={<HeartOutlined />}>Favoritos</Menu.Item>
                                <Menu.Item key="3" icon={<DeleteOutlined />}> <Link to="/Trash"> Papelera </Link>  </Menu.Item>
                                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>


                            </Menu>


                        </div>

                        <div id="Title">
                            {/* <AiOutlineFile size={30} /> */}
                            <h2 > No hay notas favoritas</h2>
                        </div>



                      


                    </div>
                )
            }
        } else {
            return (
                <Redirect to="/" />
            )
        }

    }

}




function ModalMenu(props) {
    return (
        <Modal {...props} title="EDITAR NOTA" width={800} >

        </Modal>
    );
}












