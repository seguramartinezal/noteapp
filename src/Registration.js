import React from "react";
import {Form} from "react-bootstrap";
import "./Registration.css";
import {Link} from "react-router-dom";
import axios from "axios";
import {ToastContainer, toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export const URL = "http://localhost:4000/Registro";

export default class Registro extends React.Component{


    constructor(){
        super()
        this.state = {Name: String, LastName: String, User: String, Password:String, PasswordC: String, Result: String}
    }

    handleInput = ({target}) => {

        const {name, value} = target;
        this.setState({ [name]: value });

    }

    handleForm = async (e) => {
        e.preventDefault();

        let objectinformation = {
            Name: this.state.Name,
            LastName: this.state.LastName,
            User: this.state.User,
            Password: this.state.Password,
            PasswordC: this.state.PasswordC
        }

        console.log(objectinformation);

        const res = await axios.post(URL, objectinformation);
        this.setState({Result: res});
        this.reactNotification();

        if(this.state.Result.data){
        let form = e.target;
        form.reset();
        
        }

    }


    reactNotification = () => {

        this.state.Result.data === true ? toast("Datos Guardados Correctamente", {autoClose:3000, type:"success"}) : toast("Contraseña Incorrecta", {autoClose:3000, type:"error"});
    }


    render(){
    return (
        <div>
              <div id="ContenedorRegis">
                <h2 id="TituloRegis">Registro</h2>

                <form className="form control" onSubmit={this.handleForm}>

                <b><i>Nombre</i></b> <Form.Control type="text" name="Name" id="inputName" onChange={this.handleInput}/> <br/>
                <b><i>Apellido</i></b> <Form.Control type="text" name="LastName" id="inputLastName" onChange={this.handleInput}/> <br/>
                <b><i>Nombre de Usuario</i></b> <Form.Control type="text" name="User" id="inputUser" onChange={this.handleInput}/> <br/>
                <b><i>Contraseña</i></b>  <Form.Control type="Password"  name="Password" id="inputPassword" onChange={this.handleInput}/><br/>
                <b><i>Confirmar Contraseña</i></b>  <Form.Control type="Password"  name="PasswordC" id="inputPasswordConfirmation" onChange={this.handleInput}/><br/>

                <div className="buttonvolver">
                
                    <Link to={"./"} style={{color: 'white', textDecoration: 'none'}}>
                         <button className="btn btn-success">Regresar</button>
                    </Link>
                </div>

                <div className="buttonguardar">

            
                    <button className="btn btn-primary" type="submit"> Guardar</button>
              
                </div>

                </form>
                </div>
                <ToastContainer/>
        </div>
         )
    }
}