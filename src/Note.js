import React from "react";
import "./Note.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Menu, Button, Modal, Dropdown } from "antd";
import "./Menu.css";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
  HeartOutlined,
  DeleteOutlined,
  PoweroffOutlined
} from '@ant-design/icons';
import { GoSearch } from "react-icons/all";
import { Container, Nav, Navbar, NavbarBrand, Form } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import Mansory from "react-masonry-css";
// import { IDusuario } from "./Form";


export const URL = "http://localhost:4000/Notes";
const URLuser = "http://localhost:4000/getid";
//let idsession = sessionStorage.getItem('ID');
 let idsession;

const initialState = {
  show: false,
  collapsed: false,
  activeKey: null,
  Title: "",
  Note: [],
  //idUsuario: Number,
  result: Boolean,
  data: [],
}

const initialStateModal = {
  loadTitle: String,
  loadNote: String,
  loadId: Number
}



export default class NoteBook extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      show: false,
      collapsed: false,
      activeKey: null,
      Title: "",
      Note: [],
      //idUsuario: Number,
      result: Boolean,
      data: [],
      modal: false,
      closeButtonModal: false,
      loadTitle: String,
      loadNote: String,
      loadId: Number,
      datacopy: [],
      session: true,
      idusuario: Number
      


    }

    this.handleChange = this.handleChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.toggleCollapsed = this.toggleCollapsed.bind(this);
    this.handleDiv = this.handleDiv.bind(this);
    this.getAllData = this.getAllData.bind(this);
    this.showModal = this.showModal.bind(this);
    this.loadDataModal = this.loadDataModal.bind(this);
    this.handleOk = this.handleOk.bind(this);
    this.handleInformation = this.handleInformation.bind(this);
    this.resetState = this.resetState.bind(this);
    this.reactNotification = this.reactNotification.bind(this);
  }

  componentDidMount() {
   
    this.getAllData();
  }


  getAllData = () => {

   
    
    axios.get(URL).then(response => {
      this.setState({ data: response.data.rows, datacopy: response.data.rows });
      console.log(this.state.data);
    });

    

  }


  
  // getAllData = async () => {
  //    await axios.get(URLuser).then(response => {
  //     this.setState({idusuario: response.data.id});
  //   });
   

  //   await axios.get(URL, {params: {id: this.state.idusuario}}).then(response => {
  //     this.setState({ data: response.data.rows, datacopy: response.data.rows });
  //   });


  // }


  toggleCollapsed = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  handleDiv = () => {
    this.setState({ show: !this.state.show });
  }

  handleClick = (e) => {
    this.setState({
      activeKey: e
    });

  }

  showModal = (obj) => {
    this.loadDataModal(obj);
    this.setState({ modal: !this.state.modal });


  }

  loadDataModal = (obj) => {
    this.setState({ loadId: obj.ID, loadTitle: obj.Titulo, loadNote: obj.Contenido, Title: obj.Titulo, Note: obj.Contenido });

  }

  updateDataNote = async () => {

    const NoteInfo = {
      id: this.state.loadId,
      title: this.state.loadTitle,
      content: this.state.loadNote
    }

    if (this.state.Title.length !== this.state.loadTitle.length ||
      this.state.Note.length !== this.state.loadNote.length) {

      let res = await axios.put("http://localhost:4000/Notes/:?id=" + this.state.loadId, NoteInfo);
      this.setState({ result: res.data });
      this.getAllData();
      this.reactNotificationModal();
    }
  }

  handleOk = () => {
    this.updateDataNote();
    this.setState({ modal: !this.state.modal });
  };


  handleCancel = () => {

    this.setState({ modal: !this.state.modal });
    this.setState(initialStateModal);

  };


  handleChange = ({ target }) => {
    const { name, value } = target;
    this.setState({ [name]: value });

  }

  handleInformation = async (e) => {
    let idsession = sessionStorage.getItem('ID');

    e.preventDefault();
    const NoteInfo = {
      title: this.state.Title,
      content: this.state.Note,
      place: 0,
      idUser: idsession
    }

    this.handleDiv();


    let res = await axios.post(URL, NoteInfo);
    this.setState({ result: res.data });
    this.reactNotification();
    this.getAllData();
    this.resetState();


  }

  resetState = () => {
    this.setState(initialState);
  }

  handleMenu = () => {
    console.log("mensaje");
  }

  addFavorite = async (obj) => {
    let idsession = sessionStorage.getItem('ID');
   // console.log(this.state.idusuario);

    const NoteInfo = {
      id: obj.ID,
      place: 1,
      idUser: idsession
    }


    let res = await axios.put("http://localhost:4000/Notes/", NoteInfo);
    this.setState({ result: res.data });
    this.getAllData();
    this.reactNotificationFavorite();
    this.resetState();

  }

  addTrash = async (obj) => {
    let idsession = sessionStorage.getItem('ID');

    const NoteInfo = {
      id: obj.ID,
      place: 2,
      idUser: idsession
    }


    let res = await axios.put("http://localhost:4000/Notes/", NoteInfo);
    this.setState({ result: res.data });
    this.getAllData();
    this.reactNotificationTrash();
    this.resetState();

  }


  filterNote = (event) => {
    event.preventDefault();
    let text = event.target.value.toLowerCase();
    let arrayData = [];

    if (text) {

      arrayData = this.state.data.filter((record) => record.Titulo.toLowerCase().includes(text) || record.Contenido.toLowerCase().includes(text));

      this.setState({ data: arrayData });

    } else {


      this.setState({ data: this.state.datacopy });
    }
  }

  closeSession = () => {


    if (this.state.session) {
   //   console.log(idsession);
      idsession = sessionStorage.removeItem('ID');
      this.setState({ session: false });

    }
  }


  reactNotificationTrash = () => {

    this.state.result === true ? toast("Nota agregada a la papelera.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error", { autoClose: 2000, type: "info", theme: "dark" });
  }


  reactNotificationFavorite = () => {

    this.state.result === true ? toast("Nota agregada a favorito.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error", { autoClose: 2000, type: "info", theme: "dark" });
  }


  reactNotificationModal = () => {

    this.state.result === true ? toast("Nota actualizada correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Error.", { autoClose: 2000, type: "info", theme: "dark" });
  }



  reactNotification = () => {

    this.state.result === true ? toast("Nota guardada correctamente.", { autoClose: 3000, type: "success", theme: "dark" }) : toast("Por favor, intenta llenar los campos.", { autoClose: 3000, type: "error", theme: "dark" });
  }


  render() {


    if (this.state.session) {


      if (this.state.show === false) {

        return (


          <div>


            <ToastContainer />

            <Navbar bg="dark" variant="dark">

              <Container id="Contenedor">
                <NavbarBrand>NoteApp</NavbarBrand>
                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton">
                  {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                </Button>


                <Nav className="me-auto">

                </Nav>

                <Form>

                  {/* <Form.Control type="text" placeholder="Ingresa texto para buscar..." id="Barra" className="form-control-lg"/>   */}

                  <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} />
                  </button>


                  <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" name="filter" onKeyUp={this.filterNote} />
                </Form>

              </Container>
            </Navbar>



            <div className="nav-wrapper">

            </div>

            <div style={{ width: 256 }}>



              <Menu defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>
                <Menu.Item key="1" icon={<PieChartOutlined />}> Notas </Menu.Item>
                <Menu.Item key="2" icon={<HeartOutlined />}> <Link to="/Favorites"> Favoritos </Link> </Menu.Item>
                <Menu.Item key="3" icon={<DeleteOutlined />}><Link to="/Trash"> Papelera </Link></Menu.Item>
                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>
              </Menu>

            </div>


            <div className="takenotes">
              <input type="text" placeholder="Tomar una nota..." name="Title" onClick={this.handleDiv} onChange={() => null} autoComplete="off" />
            </div> <br />

            {this.state.data.map((info, id) =>
              <Mansory className="my-masonry-grid" columnClassName="my-masonry-grid_column" key={id} >

                <div className="row"  >
                  <div onClick={() => this.showModal(info)}>

                    <div id="id" >{info.ID}</div>
                    <div className="artigo_nome1">{info.Titulo}</div>
                    <div className="artigo_nome" >{info.Contenido}</div>

                  </div>
                  <Dropdown.Button overlay={

                    <Menu>
                      <Menu.Item onClick={() => this.addFavorite(info)} key={1} icon={<HeartOutlined />}>Agregar a favorito</Menu.Item>
                      <Menu.Item onClick={() => this.addTrash(info)} key={2} icon={<DeleteOutlined />}>Mover a la papelera</Menu.Item>
                    </Menu>
                  } type="text" className="MenuButton"></Dropdown.Button>




                </div>


              </Mansory>
            )}


            <ModalMenu visible={this.state.modal} onOk={this.handleOk} onCancel={this.handleCancel} closable={this.state.closeButtonModal} onChange={this.handleChange}>

              <textarea type="text" placeholder="Titulo" name="loadTitle" onChange={this.handleChange} autoFocus autoComplete="off" value={this.state.loadTitle} /><br /><br />

              <textarea type="text" placeholder="Tomar una nota..." name="loadNote" onChange={this.handleChange} autoComplete="off" value={this.state.loadNote} /><br />


            </ModalMenu>



          </div>

        )

      } else {

        return (

          <div>

            <ToastContainer />
            <Navbar bg="dark" variant="dark">

              <Container id="Contenedor">
                <NavbarBrand>NoteApp</NavbarBrand>
                <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }} id="Menubutton">
                  {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                </Button>

                <Nav className="me-auto">

                </Nav>


                <Form>


                  <button className="btn btn-primary" id="Buscar"> <GoSearch id="Lupa" size={22} />
                  </button>

                  <input className="form-control" type="text" placeholder="Escribe texto para buscar..." aria-label=".form-control-lg example" id="Barra" autoComplete="off" onKeyUp={this.filterNote} />
                </Form>

              </Container>
            </Navbar>

            <div className="nav-wrapper"> </div>

            <div style={{ width: 256 }}>

              <Menu defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']} mode="inline" theme="light" inlineCollapsed={this.state.collapsed}>

                <Menu.Item key="1" icon={<PieChartOutlined />}> Notas </Menu.Item>
                <Menu.Item key="2" icon={<HeartOutlined />}> <Link to="/Favorites"> Favoritos </Link> </Menu.Item>
                <Menu.Item key="3" icon={<DeleteOutlined />}><Link to="/Trash"> Papelera </Link></Menu.Item>
                <Menu.Item key="4" icon={<PoweroffOutlined />} onClick={this.closeSession}> Cerrar Sesion </Menu.Item>

              </Menu>
            </div>


            {/* <div className="takenotes2" > */}
            <form className="form-group" onSubmit={this.handleInformation}>
              <input type="text" placeholder="Titulo" name="Title" onChange={this.handleChange} autoFocus autoComplete="off" /><br /><br />
              <input type="text" placeholder="Tomar una nota..." name="Note" onChange={this.handleChange} autoComplete="off" /><br />
              <button id={'cerrar'} type="submit">Cerrar</button>
            </form>

            {/* </div> */}


            {this.state.data.map((info, id) =>
              <Mansory className="my-masonry-grid" columnClassName="my-masonry-grid_column" key={id}>

                <div className="row"  >
                  <div onClick={() => this.showModal(info)}>

                    <div id="id" >{info.ID}</div>
                    <div className="artigo_nome1">{info.Titulo}</div>
                    <div className="artigo_nome" >{info.Contenido}</div>

                  </div>
                  <Dropdown.Button overlay={

                    <Menu>
                      <Menu.Item onClick={() => this.addFavorite(info)} key={1} icon={<HeartOutlined />}>Agregar a favorito</Menu.Item>
                      <Menu.Item onClick={() => this.addTrash(info)} key={2} icon={<DeleteOutlined />}>Mover a la papelera</Menu.Item>
                    </Menu>
                  } type="text" className="MenuButton"></Dropdown.Button>

                </div>





              </Mansory>
            )}


            <ModalMenu visible={this.state.modal} onOk={this.handleOk} onCancel={this.handleCancel} closable={this.state.closeButtonModal} onChange={this.handleChange} >

              <textarea type="text" placeholder="Titulo" name="loadTitle" onChange={this.handleChange} autoFocus autoComplete="off" value={this.state.loadTitle} /><br /><br />

              <textarea type="text" placeholder="Tomar una nota..." name="loadNote" onChange={this.handleChange} autoComplete="off" value={this.state.loadNote} /><br />




            </ModalMenu>




            {/* <Mansory className="my-masonry-grid" columnClassName="my-masonry-grid_column" >

          
          <div class="artigo_nome">Computador Apple Imacdddddddddddddddd</div>  
          </Mansory> */}


            {/* {this.state.data.map((info, id) =>
          <div class="masonry">
            <div class="item">
                <span key={id} id="span1">{info.Titulo}</span>
                <span id="span2">{info.Contenido}</span>
            </div>
          </div>

          )} */}




          </div>


        )

      }
    } else {
      return (
        <Redirect to="/" />
      )
    }

  }

};









function ModalMenu(props) {
  return (
    <Modal {...props} title="EDITAR NOTA" width={800} >

    </Modal>
  );
}

// function NoteMenu(props){

//   return (

//    <Menu>
//    <Menu.Item {...props} key={1}>Agregar a favorito</Menu.Item>
//    <Menu.Item {...props} key={2}>Mover a la papelera</Menu.Item>
//  </Menu>
//   )
// }